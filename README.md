### Toptal Project

This is project for applying to Toptal

To install necessary packages

```
scripts/bootstrap
```


#### Backend development

Backend is developed on Google App Engine stack

To start development server

```
cd src/backend
source venv/bin/active
dev_appserver.py . --port=3000
```


#### Frontend development

Frontend is developed on React/Redux

To start development server

```
cd src/frontend
npm start
```


After start both backend and frontend server, you could go to [http://localhost:3000](http://localhost:3000) to see the result


#### Online Demo

This application is deployed at [http://kakaolabs-1017.appspot.com/](http://kakaolabs-1017.appspot.com/)
