import sys
import subprocess
from fabric.api import local


RED = '\033[31m'
GREEN = '\033[32m'
RESET = '\033[0m'


def run_process_asynchronous(command):
    local(command)


def run_process(command):
    process = subprocess.Popen(
        command, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    return out, err


def print_color(color, message):
    print '%s%s%s' % (color, message, RESET)


def info(message):
    print_color(GREEN, message)


def alert(message):
    print_color(RED, message)


class CoverageChecker(object):

    def _run_test(self):
        command = 'find . -name *.pyc | xargs rm -rf && rm -rf .coverage htmlcov && $VIRTUAL_ENV/bin/python src/backend/manage.py test src/backend/tests --with-coverage --failfast'  # NOQA
        run_process_asynchronous(command)

    def _run_coverage_report(self):
        command = '$VIRTUAL_ENV/bin/coverage report'
        out, _ = run_process(command)
        return out

    def _parse_coverage_report(self, content):
        lines = content.splitlines()
        try:
            self.coverage_number = int(lines[-1].split()[-1][:-1])
        except:
            self.coverage_number = 100

    def _generate_html_report(self):
        command = '$VIRTUAL_ENV/bin/coverage html && open htmlcov/index.html'
        run_process(command)

    def execute(self):
        self._run_test()
        out = self._run_coverage_report()
        self._parse_coverage_report(out)
        if self.coverage_number < 80:
            alert('Stop!!!! Our coverage is only %s' % self.coverage_number)
            self._generate_html_report()
            sys.exit(1)
        else:
            info('You done a good job')


if __name__ == '__main__':
    CoverageChecker().execute()
