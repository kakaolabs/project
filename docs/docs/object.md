# Object attributes


### User attributes

Attribute             | Description
--------------------- | -------------------------
id                    | **readonly field** string
username              | string
name                  | string
password              | string
access_token          | **readonly field** string  - Access token only return to owner of this account
role_type             | integer
created_at            | **readonly field** datetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
updated_at            | **readonly field** datetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
status                | integer


`role_type` could be:

+ 0 - normal user
+ 1 - manager user (user who could manager users)
+ 2 - admin user (user who could manager user and all of objects in system)

`status` could be:

+ 0 - valid user
+ 1 - deleted user


### Record attributes


Attribute             | Description
--------------------- | -------------------------
id                    | **readonly field** string
user                  | **readonly field** [user attributes](#user-attributes)
distance              | float
duration              | integer - total seconds
average_speed         | float
start_at              | datetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
ended_at              | datetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
created_at            | **readonly field** datetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
updated_at            | **readonly field** datetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
status                | integer

`status` could be:

+ 0 - valid record
+ 1 - deleted record


### Report attributes


Attribute             | Description
--------------------- | -------------------------
id                    | **readonly field** sstring
user                  | **readonly field** s[user attributes](#user-attributes)
total_duration        | **readonly field** sfloat - total seconds
total_distance        | **readonly field** sfloat
average_speed         | **readonly field** sfloat
start_at              | **readonly field** sdate - Date string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
ended_at              | **readonly field** sdate - Date string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
created_at            | **readonly field** sdatetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
updated_at            | **readonly field** sdatetime - Date time string which follow [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
