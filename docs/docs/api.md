# API documents for Toptal

## User sign up

POST `v1/accounts/signup`

Get parameters

+ `api_key`
+ `api_sig`

Post data

+ `name`
+ `username`
+ `password`

Error types

+ `InvalidAPIKeyError`
+ `InvalidSignatureError`
+ `ValidationError`

Return

[user object](./object/#user-attributes)


## User sign in

POST `v1/accounts/signin`

Get parameters

+ `api_key`
+ `api_sig`

Post data

+ `username`
+ `password`

Error types

+ `InvalidAPIKeyError`
+ `InvalidSignatureError`
+ `ValidationError`

Return

[user object](./object/#user-attributes)


## User refersh token

POST `v1/user/me/refresh_token`

Get parameters

+ `access_token`

Post data

+ `refresh_token`

Error types

+ `AuthenticationFailedError`
+ `ResourceNotFoundException`

Return

[user object](./object/#user-attributes)


## User get current user

GET `v1/user/me`

Get parameters

+ `access_token`


Error types

+ `AuthenticationFailedError`

Return

[user object](./object/#user-attributes)


## Update User

PUT `v1/users/me`

Get parameters

+ `access_token`

Post data

[user object](./object/#user-attributes)

Error types

+ `InvalidAccessToken`
+ `PermissionDeniedError`

Return

[user object](./object/#user-attributes)


## Create record

POST `v1/records`

Get parameters

+ `access_token`

Post data

[record object](./object/#record-attributes)

Error types

+ `InvalidAccessToken`

Return

[record object](./object/#record-attributes)


## Update record

PUT `v1/records/:record_id`

Get parameters

+ `access_token`

Post data

[record object](./object/#record-attributes)

Error types

+ `InvalidAccessToken`

Return

[record object](./object/#record-attributes)


## List all records

GET `v1/records`

Get parameters

+ `access_token`

Error types

+ `InvalidAccessToken`

Return

List of [record objects](./object/#record-attributes)
This API has pagination


## List all report

GET `v1/reports`

Get parameters

+ `access_token`

Error types

+ `InvalidAccessToken`

Return

List of [report objects](./object/#report-attributes)
This API has pagination
