from google.appengine.ext import ndb

from rest_framework.filters import BaseFilterBackend
from rest_framework.fields import DateTimeField, DateField
from rest_framework.serializers import Serializer


class NDBFilterBackend(BaseFilterBackend):
    """ Allow cstool could easily to query into database using filter
    Filter includes 2 fields
    + `query` is list of items. Each items include 2 fields:
        - `operator: '=', '!=', '>', '>=', '<', '<=', 'AND', 'OR'
        - operand: List of object. Operand could also be a filter item
    + `order`: list of item. Each item includes 2 fields:
        - `name`: field name
        - `asc`: true or false
    + `selected_fields`: list of field client want to retrieve
    """

    def get_filters(self, request):
        result = request.query_params.get('filter')
        if not result:
            result = request.data.get('filter')
        return result

    def get_op2(self, prop, item):
        op2 = item['operands'][1]
        if isinstance(prop, ndb.KeyProperty):
            return ndb.Key(urlsafe=op2)
        if isinstance(prop, ndb.DateTimeProperty):
            return DateTimeField.to_internal_value(op2)
        return op2

    def get_filter_node(self, item, model_class):
        operator = item['operator']
        if operator not in ['AND', 'OR', 'IN']:
            op1 = item['operands'][0]
            prop = model_class._properties[op1]
            op2 = self.get_op2(prop, item)
            return prop._comparison(operator, op2)
        elif operator == 'IN':
            op1 = item['operands'][0]
            prop = model_class._properties[op1]
            op2 = self.get_op2(prop, item)
            return prop._IN(op2)
        elif operator == 'AND':
            return ndb.AND(*[
                self.get_filter_node(sub_item, model_class)
                for sub_item in item['operands']])
        elif operator == 'OR':
            return ndb.OR(*[
                self.get_filter_node(sub_item, model_class)
                for sub_item in item['operands']])

    def get_filters_node(self, filters, model_class):
        if 'query' in filters:
            return ndb.AND(*[
                self.get_filter_node(item, model_class)
                for item in filters['query']])

    def get_order_node(self, item, model_class):
        field = item['name']
        prop = model_class._properties[field]
        if item['asc']:
            return prop
        else:
            return -prop

    def get_orders(self, filters, model_class):
        if 'orders' in filters:
            return [
                self.get_order_node(item, model_class)
                for item in filters['orders']]

    def get_selected_fields(self, filters):
        if 'selected_fields' not in filters:
            return None
        raw_fields = filters['selected_fields']
        data = {}
        for field in raw_fields:
            nested_fields = field.split('.')
            value = data
            for field in nested_fields[:-1]:
                if field not in value:
                    value[field] = {}
                value = value[field]
            value[nested_fields[-1]] = True
        return data

    def filter_queryset(self, request, queryset, view):
        filters = self.get_filters(request)
        if not filters:
            return None, queryset

        query = queryset
        filter_node = self.get_filters_node(filters, view.model_class)
        if filter_node:
            query = query.filter(filter_node)

        orders = self.get_orders(filters, view.model_class)
        if orders:
            query = query.order(*orders)

        selected_fields = self.get_selected_fields(filters)
        return selected_fields, query


class BaseStartAtFilter(BaseFilterBackend):

    filter_serializer_class = None

    def filter_queryset(self, request, queryset, view):
        s = self.filter_serializer_class(data=request.query_params)
        if not s.is_valid():
            return queryset

        prop = getattr(view.model_class, 'start_at')
        if 'start' in s.validated_data:
            queryset = queryset.filter(prop >= s.validated_data['start'])
        if 'end' in s.validated_data:
            queryset = queryset.filter(prop < s.validated_data['end'])
        return queryset.order(-prop)


class DateSerializer(Serializer):

    start = DateField(required=False)
    end = DateField(required=False)


class StartAtDateFilter(BaseStartAtFilter):

    filter_serializer_class = DateSerializer


class DateTimeSerializer(Serializer):

    start = DateTimeField(required=False)
    end = DateTimeField(required=False)


class StartAtDateTimeFilter(BaseStartAtFilter):

    filter_serializer_class = DateTimeSerializer


class UserKeyFilter(BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        try:
            user_key = ndb.Key(urlsafe=request.query_params.get('user_key'))
            if user_key:
                prop = getattr(view.model_class, 'user_key')
                queryset = queryset.filter(prop == user_key)
        except:
            pass

        return queryset


class UserNameFilter(BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        try:
            user_key = ndb.Key('User', request.query_params.get('username'))
            if user_key:
                prop = getattr(view.model_class, 'user_key')
                queryset = queryset.filter(prop == user_key)
        except:
            pass

        return queryset
