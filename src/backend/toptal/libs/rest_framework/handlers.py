import traceback
import settings

from rest_framework.response import Response


def response_success(data):
    headers = {'Content-Type': 'application/json'}
    content = {'data': data}
    return Response(content, headers=headers)


def error_message(errors):
    if isinstance(errors, dict):
        key = errors.keys()[0]
        error = errors[key]
        if isinstance(error, dict):
            return error_message(error)
        if isinstance(error, list):
            return '%s: %s' % (key, error[0])
    elif isinstance(errors, list):
        for error in errors:
            if error and isinstance(error, dict):
                return error_message(error)


def response_validation_error(errors):
    headers = {'Content-Type': 'application/json'}
    content = {
        'error': {
            'type': 'ValidationError',
            'message': error_message(errors)
        }
    }
    return Response(content, headers=headers, status=402)


def handle_exception(exc, context):
    headers = {'Content-Type': 'application/json'}
    content = {
        'error': {
            'type': exc.__class__.__name__,
            'message': exc.message,
        }
    }
    status_code = getattr(exc, 'status_code', 402)
    if settings.DEBUG:  # pragma: no cover
        trace_log = traceback.format_exc()
        content['error']['debug'] = trace_log
    return Response(content, headers=headers, status=status_code)
