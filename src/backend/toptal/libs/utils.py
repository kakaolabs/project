import datetime
import uuid as original_uuid


def uuid():
    return str(original_uuid.uuid4()).replace('-', '')


def now():
    return datetime.datetime.now()


def now_delta(**kwargs):
    return now() + datetime.timedelta(**kwargs)


def start_week_of(day):
    day_of_week = day.weekday()
    start_week = day - datetime.timedelta(days=day_of_week)
    return start_week.date()


def end_week_of(day):
    day_of_week = day.weekday()
    end_week = day + datetime.timedelta(days=6 - day_of_week)
    return end_week.date()


def lazy(func):
    attr_name = '_lazy_%s' % func.__name__

    @property
    def wrapper(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, func(self))
        return getattr(self, attr_name)

    @wrapper.setter
    def wrapper(self, value):
        setattr(self, attr_name, value)

    return wrapper
