from rest_framework.routers import SimpleRouter
from views import (
    UserViewSet,
    EntryViewSet,
    ReportViewSet)


router = SimpleRouter(trailing_slash=False)
router.register(r'users', UserViewSet, base_name='admin-users')
router.register(r'entries', EntryViewSet, base_name='admin-entries')
router.register(r'reports', ReportViewSet, base_name='admin-entries')

urlpatterns = router.urls
