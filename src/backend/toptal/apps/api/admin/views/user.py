from google.appengine.ext import ndb

from toptal.libs.rest_framework import handlers
from toptal.exceptions import PermissionDeniedError
from toptal.apps.core.models import User
from toptal.apps.api.admin.authentications import AccessTokenUserManagerAuthentication  # noqa
from toptal.apps.api.admin.serializers import (
    UserSerializer,
    UserManagerUpdateSerializer,
    UserAdminUpdateSerializer)
from toptal.apps.core.services import (
    UserCreateService,
    UserUpdateService)

from base import ToptalBaseAdminViewSet


class ListMixin(object):

    def list(self, request):
        username = request.query_params.get('username')
        if username:
            user = ndb.Key('User', username).get()
            page = [user] if user else []
            s = UserSerializer(
                page, many=True,
                context={'request': request, 'user': request.user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        else:
            queryset = User.query(User.status == User.USER_STATUS_NORMAL)
            page = self.paginate_queryset(queryset)
            s = UserSerializer(
                page, many=True,
                context={'request': request, 'user': request.user})
            s.selected_fields = self.get_selected_fields(request)
            return self.get_paginated_response(s.data)


class CreateMixin(object):

    def create(self, request):
        if request.user.role_type == User.USER_ROLE_TYPE_ADMIN:
            klass = UserAdminUpdateSerializer
        else:
            klass = UserManagerUpdateSerializer
        s = klass(data=request.data)
        if s.is_valid():
            user = UserCreateService(**s.validated_data).execute()
            s = UserSerializer(
                user, context={'request': request, 'user': request.user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)


class RetrieveMixin(object):

    def retrieve(self, request, pk):
        user = self.get_object(pk)
        s = UserSerializer(
            user, context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return handlers.response_success(s.data)


class UpdateMixin(object):

    def update(self, request, pk):
        user = self.get_object(pk)
        self.check_permission(user, request)
        if request.user.role_type == User.USER_ROLE_TYPE_ADMIN:
            klass = UserAdminUpdateSerializer
        else:
            klass = UserManagerUpdateSerializer
        s = klass(data=request.data, partial=True)
        if s.is_valid():
            new_user = UserUpdateService(user, **s.validated_data).execute()
            s = UserSerializer(
                new_user, context={'request': request, 'user': request.user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)


class DeleteMixin(object):

    def destroy(self, request, pk):
        user = self.get_object(pk)
        self.check_permission(user, request)
        new_user = UserUpdateService(
            user, status=User.USER_STATUS_DELETED).execute()
        s = UserSerializer(
            new_user, context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return handlers.response_success(s.data)


class UserViewSet(
        ListMixin,
        CreateMixin,
        RetrieveMixin,
        UpdateMixin,
        DeleteMixin,
        ToptalBaseAdminViewSet):

    authentication_classes = [AccessTokenUserManagerAuthentication]

    def check_permission(self, user, request):
        is_owner = user.id == request.user.id
        has_permission = request.user.role_type > user.role_type
        if not is_owner and not has_permission:
            raise PermissionDeniedError(
                'You could not update user who has more power than yours')
