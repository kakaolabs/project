from rest_framework.routers import SimpleRouter
from views import (
    AccountViewSet,
    UserViewSet,
    CurrentUserViewSet,
    EntryViewSet,
    ReportViewSet)


router = SimpleRouter(trailing_slash=False)
router.register(r'accounts', AccountViewSet, base_name='v1-accounts')
router.register(r'users', UserViewSet, base_name='v1-users')
router.register(r'users/me', CurrentUserViewSet, base_name='v1-current-users')
router.register(r'entries', EntryViewSet, base_name='v1-entries')
router.register(r'reports', ReportViewSet, base_name='v1-users')

urlpatterns = router.urls
