from rest_framework.authentication import BaseAuthentication

from toptal.apps.core.models import AccessToken
from toptal.exceptions import AuthenticationFailedError


class AccessTokenAuthentication(BaseAuthentication):

    def authenticate(self, request):
        access_token = request.query_params.get('access_token')
        if access_token:
            token = AccessToken.get_by_id(access_token)
            if token and token.is_valid():
                return (token.user_key.get(), token)
        raise AuthenticationFailedError('Invalid access token')
