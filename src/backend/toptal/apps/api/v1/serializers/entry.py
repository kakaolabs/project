from rest_framework import fields
from rest_framework.exceptions import ValidationError

from toptal.apps.core.models import Entry, Report
from toptal.libs import utils
from toptal.libs.rest_framework.serializers import ModelSerializer

from user import UserSerializer


class EntrySerializer(ModelSerializer):

    id = fields.CharField(source='safe_id')
    user = UserSerializer(source='user_async')
    duration = fields.IntegerField()
    average_speed = fields.FloatField()

    class Meta:
        model = Entry
        fields = (
            'id',
            'user',
            'distance',
            'duration',
            'average_speed',
            'start_at',
            'end_at',
            'created_at',
            'updated_at',
            'status',
        )


class EntryCreateSerializer(ModelSerializer):

    class Meta:
        model = Entry
        fields = (
            'user_key',
            'distance',
            'start_at',
            'end_at',
        )

    def validate_entry_in_week(self, data):
        d1 = utils.start_week_of(data['start_at'])
        d2 = utils.start_week_of(data['end_at'])
        if d1 != d2:
            raise ValidationError('start at and end at are not in same week')

    def validate(self, data):
        self.validate_entry_in_week(data)
        return data


class EntryUpdateSerializer(ModelSerializer):

    class Meta:
        model = Entry
        fields = (
            'distance',
            'start_at',
            'end_at',
        )

    def validate_entry_in_week(self, data):
        if 'start_at' in data:
            d1 = utils.start_week_of(data['start_at'])
        else:
            d1 = self.instance.start_week_at

        if 'end_at' in data:
            d2 = utils.start_week_of(data['end_at'])
        else:
            d2 = utils.start_week_of(self.instance.end_at)

        if d1 != d2:
            raise ValidationError('start at and end at are not in same week')

    def validate(self, data):
        if 'start_at' in data or 'end_at' in data:
            self.validate_entry_in_week(data)
        return data


class ReportSerializer(ModelSerializer):

    id = fields.CharField(source='safe_id')
    user = UserSerializer(source='user_async')
    average_speed = fields.FloatField()

    class Meta:
        model = Report
        fields = (
            'id',
            'user',
            'total_distance',
            'total_duration',
            'average_speed',
            'start_at',
            'end_at',
            'created_at',
            'updated_at',
            'status',
        )
