from rest_framework.decorators import list_route

from toptal.libs.rest_framework import handlers
from toptal.apps.api.v1.authentications import AccessTokenAuthentication
from toptal.apps.api.v1.serializers import (
    UserRefreshTokenSerializer,
    UserSerializer)
from toptal.apps.core.services import UserRefreshTokenService

from base import ToptalBaseViewSet


class RefreshTokenMixin(object):

    @list_route(methods=['post'])
    def refresh_token(self, request):
        s = UserRefreshTokenSerializer(data=request.data)
        if s.is_valid():
            user, token = UserRefreshTokenService(
                request.auth, s.validated_data['refresh_token']).execute()
            user.acccess_token = token
            s = UserSerializer(
                user, context={'request': request, 'user': user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)


class CurrentUserViewSet(
        RefreshTokenMixin,
        ToptalBaseViewSet):

    authentication_classes = [AccessTokenAuthentication]
