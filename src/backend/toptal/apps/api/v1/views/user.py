from rest_framework.decorators import list_route

from toptal.libs.rest_framework import handlers
from toptal.apps.api.v1.authentications import AccessTokenAuthentication
from toptal.apps.api.v1.serializers import (
    UserSerializer,
    UserUpdateSerializer)
from toptal.apps.core.services import UserUpdateService

from base import ToptalBaseViewSet


class UserViewSet(ToptalBaseViewSet):

    authentication_classes = [AccessTokenAuthentication]

    @list_route(methods=['get', 'put'])
    def me(self, request):
        if request.method == 'GET':
            return self.retrieve(request)
        else:
            return self.update(request)

    def update(self, request):
        s = UserUpdateSerializer(data=request.data, partial=True)
        if s.is_valid():
            UserUpdateService(request.user, **s.validated_data).execute()
            s = UserSerializer(
                request.user,
                context={'request': request, 'user': request.user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)

    def retrieve(self, request):
        request.user.access_token = request.auth
        s = UserSerializer(
            request.user,
            context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return handlers.response_success(s.data)
