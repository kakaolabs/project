from google.appengine.ext import ndb


class BaseModel(ndb.Model):

    created_at = ndb.DateTimeProperty(auto_now_add=True)
    updated_at = ndb.DateTimeProperty(auto_now=True)

    @classmethod
    @ndb.tasklet
    def create_async(cls, **kwargs):
        instance = cls(**kwargs)
        yield instance.put_async()
        raise ndb.Return(instance)

    @classmethod
    def create(cls, **kwargs):
        return cls.create_async(**kwargs).get_result()

    @property
    def id(self):
        return self.key.id() if self.key else None

    @property
    def safe_id(self):
        return self.key.urlsafe() if self.key else None

    def build_update(self, **kwargs):
        old_data = {}
        new_data = {}
        for k, v in kwargs.iteritems():
            old_value = getattr(self, k, None)
            if old_value != v:
                new_data[k] = v
                old_data[k] = old_value
        if new_data:
            self._set_attributes(new_data)
        return old_data, new_data

    @ndb.tasklet
    def update_async(self, **kwargs):
        old_data, new_data = self.build_update(**kwargs)
        if new_data:
            yield self.put_async()
        raise ndb.Return(self, old_data, new_data)

    def update(self, **kwargs):
        return self.update_async(**kwargs).get_result()
