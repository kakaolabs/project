from google.appengine.ext import ndb

from toptal.exceptions import PermissionDeniedError
from toptal.apps.core.models import User, Entry


class EntryDeleteService(object):

    def __init__(self, user, entry):
        self.user = user
        self.entry = entry

    def check_permission(self):
        is_entry_owner = self.entry.user_key == self.user.key
        is_admin = self.user.role_type == User.USER_ROLE_TYPE_ADMIN
        if not is_entry_owner and not is_admin:
            raise PermissionDeniedError(
                'user does not have permission to edit entry')

    @ndb.tasklet
    def update_entry(self, entry):
        yield entry.update_async(status=Entry.ENTRY_STATUS_DELETED)

    @ndb.tasklet
    def update_report(self, report, entry):
        yield report.update_async(
            total_distance=report.total_distance - entry.distance,
            total_duration=report.total_duration - entry.duration)

    @ndb.transactional_tasklet(xg=True, retries=10)
    def update_entry_and_report(self):
        entry = yield self.entry.key.get_async()
        report = yield entry.report_key.get_async()
        yield (
            self.update_entry(entry),
            self.update_report(report, entry))
        raise ndb.Return(entry, report)

    @ndb.tasklet
    def execute_async(self):
        self.check_permission()
        entry, report = yield self.update_entry_and_report()
        raise ndb.Return(entry, report)

    def execute(self):
        return self.execute_async().get_result()
