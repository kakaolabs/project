from google.appengine.ext import ndb
from django.contrib.auth.hashers import make_password


class UserUpdateService(object):

    def __init__(self, user, **kwargs):
        self.user = user
        self.kwargs = kwargs

    @ndb.transactional_tasklet(retries=10)
    def execute_async(self):
        data = {k: v for k, v in self.kwargs.iteritems()}
        if 'password' in data:
            data['password'] = make_password(data['password'])
        yield self.user.update_async(**data)
        raise ndb.Return(self.user)

    def execute(self):
        return self.execute_async().get_result()
