from google.appengine.ext import ndb
from google.appengine.api.datastore_errors import TransactionFailedError
from django.contrib.auth.hashers import make_password

from toptal.exceptions import ResourceAlreadyExistError
from toptal.apps.core.models import User


class UserCreateService(object):

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    @ndb.tasklet
    def check_unique_username(self):
        user_key = ndb.Key('User', self.kwargs['username'])
        user = yield user_key.get_async()
        if user:
            raise ResourceAlreadyExistError('username is already existed')

    @ndb.transactional_tasklet(retries=1)
    def create_user(self):
        data = {k: v for k, v in self.kwargs.iteritems()}
        if 'password' in data:
            data['password'] = make_password(data['password'])
        username = data.pop('username')
        user = yield User.create_async(key=ndb.Key('User', username), **data)
        raise ndb.Return(user)

    @ndb.tasklet
    def execute_async(self):
        yield self.check_unique_username()
        try:
            user = yield self.create_user()
        except TransactionFailedError:
            raise ResourceAlreadyExistError('username is already existed')
        raise ndb.Return(user)

    def execute(self):
        return self.execute_async().get_result()
