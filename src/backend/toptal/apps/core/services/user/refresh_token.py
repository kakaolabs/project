from google.appengine.ext import ndb

from toptal.exceptions import (
    ResourceNotFoundError,
    ExpiredRefreshTokenError)
from toptal.libs import utils
from toptal.apps.core.models import AccessToken


class UserRefreshTokenService(object):

    def __init__(self, token, refresh_token):
        self.token = token
        self.refresh_token = refresh_token

    @ndb.tasklet
    def create_access_token(self, token):
        access_token = yield AccessToken.create_async(
            key=ndb.Key('AccessToken', utils.uuid()),
            user_key=token.user_key,
            expired_at=utils.now_delta(days=90),
            refresh_token=utils.uuid(),
            refresh_expired_at=utils.now_delta(days=120))
        raise ndb.Return(access_token)

    @ndb.tasklet
    def execute_async(self):
        if not self.token.is_valid_refresh():
            raise ExpiredRefreshTokenError('refresh token is expired')
        if self.token.refresh_token != self.refresh_token:
            raise ResourceNotFoundError('invalid refresh token')
        user, new_token = yield (
            self.token.user_key.get_async(),
            self.create_access_token(self.token))
        raise ndb.Return(user, new_token)

    def execute(self):
        return self.execute_async().get_result()
