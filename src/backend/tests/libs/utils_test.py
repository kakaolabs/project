import datetime
import unittest

from toptal.libs import utils


class TestUtils(unittest.TestCase):

    def test_start_week(self):
        self.assertEquals(
            datetime.date(2016, 10, 24),
            utils.start_week_of(datetime.datetime(2016, 10, 29, 10, 17)))
        self.assertEquals(
            datetime.date(2016, 10, 24),
            utils.start_week_of(datetime.datetime(2016, 10, 28, 10, 17)))
        self.assertEquals(
            datetime.date(2016, 10, 24),
            utils.start_week_of(datetime.datetime(2016, 10, 24, 10, 17)))

    def test_end_week(self):
        self.assertEquals(
            datetime.date(2016, 10, 30),
            utils.end_week_of(datetime.datetime(2016, 10, 29, 10, 17)))
        self.assertEquals(
            datetime.date(2016, 10, 30),
            utils.end_week_of(datetime.datetime(2016, 10, 28, 10, 17)))
        self.assertEquals(
            datetime.date(2016, 10, 30),
            utils.end_week_of(datetime.datetime(2016, 10, 24, 10, 17)))
