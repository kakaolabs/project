from toptal.apps.api.v1.views import CurrentUserViewSet

from tests.toptal_test import ForceAuthenticateUserTest


class TestCurrentUserViewSet(ForceAuthenticateUserTest):

    VIEW_CLASS = CurrentUserViewSet

    def test_refresh_token(self):
        res = self.client.post('/v1/users/me/refresh_token', data={
            'refresh_token': self.token.refresh_token
        })
        self.assertEquals(200, res.status_code)

    def test_update(self):
        res = self.client.put('/v1/users/me', data={
            'name': 'helllo'
        })
        self.assertEquals(200, res.status_code)

    def test_get(self):
        res = self.client.get(
            '/v1/users/me?selected_fields=access_token.value')
        self.assertEquals(200, res.status_code)
