import settings
import nose
import unittest

from toptal.exceptions import (
    InvalidAPIKeyError,
    InvalidSignatureError)
from toptal.apps.api.v1.authentications import SignatureAuthentication

from tests.toptal_test import RequestFactory


class TestSignatureAuthentication(unittest.TestCase):

    def setUp(self):
        self.authenticator = SignatureAuthentication()

    @nose.tools.raises(InvalidAPIKeyError)
    def test_wrong_api_key(self):
        request = RequestFactory().get('/', {'api_key': 'abc123'})
        self.authenticator.authenticate(request)

    @nose.tools.raises(InvalidSignatureError)
    def test_wrong_api_sig(self):
        request = RequestFactory().get('/', {
            'api_key': settings.TOPTAL_API_KEY,
        })
        self.authenticator.authenticate(request)

    def test_success_api_sig(self):
        request = RequestFactory().post('/hello?name=kiennt&age=28', data={
            'api_key': settings.TOPTAL_API_KEY,
            'api_sig': '16490b967143368e970a1696cc4cc8cf5ed758931992ba6719a8f18d1580d4af'  # noqa
        })
        self.authenticator.authenticate(request)
