import json

from google.appengine.ext import ndb

from toptal.libs import utils
from toptal.apps.api.admin.views import ReportViewSet
from toptal.apps.core.models import User
from toptal.apps.core.services import EntryCreateService

from tests.toptal_test import ForceAuthenticateUserTest


class TestReportViewSet(ForceAuthenticateUserTest):

    VIEW_CLASS = ReportViewSet

    def setUpModel(self):
        super(TestReportViewSet, self).setUpModel()
        for i in xrange(5):
            user = User.create(key=ndb.Key('User', 'test-%s' % i), name='test')
            EntryCreateService(
                user,
                user_key=user.key, distance=10,
                start_at=utils.now_delta(hours=-1),
                end_at=utils.now()).execute()

    def test_list_report(self):
        res = self.client.get('/admin-api/reports')
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(5, len(data))

    def test_list_report_with_time_filter(self):
        res = self.client.get(
            '/admin-api/reports?start=%s' % (
                utils.now_delta(hours=1).date().strftime('%Y-%m-%d')))
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(0, len(data))
