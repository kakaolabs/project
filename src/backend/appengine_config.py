import site
import os
import os.path
import sys
import monkey_patch


# andd vendor folder
def add_vendor_packages(vendor_folder):
    sys.path, remainder = sys.path[:1], sys.path[1:]
    site.addsitedir(os.path.join(os.path.dirname(__file__), vendor_folder))
    sys.path.extend(remainder)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
add_vendor_packages('vendors')

# appstats config
appstats_MAX_STACK = 100
appstats_MAX_LOCALS = 10
appstats_MAX_REPR = 100
appstats_MAX_DEPTH = 100

# money patch config
monkey_patch.patch_base_serializer()
monkey_patch.patch_is_simple_callable()
