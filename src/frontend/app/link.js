export default {
  user: (obj) => `/users/${obj.phone.slice(1)}`,
  circle: (obj) => `/circles/${obj.id}`,
  round: (obj) => `/rounds/${obj.id}`,
  broadcast: (obj) => `/broadcast/${obj.id}`,
  verifyItem: (obj) => `users/${obj.user.phone.slice(1)}/edit`,
  userUncompletedProfile: (obj) => `users/${obj.user.phone.slice(1)}/edit`,
  coupon_campaign: (obj) => `/coupon_campaign/${obj.id}`,
  coupon: (obj) => `/coupon/${obj.id}/edit`,
  role: (obj) => `/role/${obj.id}/edit`,
  admin_role: (obj) => `/role/${obj}/add_new`,
  feature: (obj) => `/features/${obj.id}`,
  user_feature: (obj) => obj.user ? `/users/${obj.user.phone.slice(1)}?tab=feature_list`: ``,
  money_task: (obj) => `/money_tasks/${obj.id}`,
};
